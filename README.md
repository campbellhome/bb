# Blackbox

---

Blackbox (bb) is a remote log viewer for C/C++ applications.  It is aimed at multiplayer games, where you might want to be collecting logs from multiple consoles simultaneously.

Blackbox stores logs in a binary format, and tracks source file/line, thread, log category, etc for each log line.

---

Usage
-----
To integrate Blackbox, simply include bb.h and link to blackbox.lib.  As an alternative to linking to blackbox.lib, you can instead include src/common/*.c in your project.

Minimal integration looks something like this:

```
#include <stdio.h>
#include "bb.h"

int main(int argc, const char **argv)
{
	BB_INIT("Application Name");
	BB_THREAD_START("Main");

	BB_LOG("Startup", "%s started with %d args\n", argv[0], argc);

	BB_SHUTDOWN();
	return 0;
}
```

Make sure bb.exe is running, to capture the session.  Then run your program.  A view should automatically open in bb.exe's window.  You can show/hide logs based on category and thread, etc.

---

Credits
-------
Developed by Matt Campbell.  Blackbox was inspired by [Deja Insight](http://www.dejatools.com/dejainsight), which I first used at [Obsidian Entertainment](https://www.obsidian.net/) while working on [Dungeon Siege III](https://en.wikipedia.org/wiki/Dungeon_Siege_III).

Blackbox grew out of a desire to have Deja Insight-like functionality on [Evolve](https://en.wikipedia.org/wiki/Evolve_(video_game)) and other projects at [Turtle Rock Studios](https://www.turtlerockstudios.com/).

Embeds [Dear ImGui](https://github.com/ocornut/imgui) by Omar Cornut (MIT License).

---

License
-------
Blackbox is licensed under the MIT License; see License.txt for more information.


