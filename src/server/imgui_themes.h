// Copyright (c) 2012-2019 Matt Campbell
// MIT license (see License.txt)

#pragma once

#include "theme_config.h"

struct ImVec4;

void Style_Init(void);
void Style_Apply(void);
void Style_Reset(const char *colorscheme);
void StyleColorsVSDark(void);
void StyleColorsWindows(void);

ImVec4 MakeColor(styleColor_e idx);
