// Copyright (c) 2012-2019 Matt Campbell
// MIT license (see License.txt)

#include "theme_config.h"
#include "cmdline.h"
#include "json_generated.h"
#include "structs_generated.h"

resolvedStyle g_styleConfig;

typedef struct tagColorName {
	s64 index;
	const char *name;
} colorName;

const colorName s_styleColorNames[] = {
	{ kStyleColor_kBBColor_Default, "kBBColor_Default" },
	{ kStyleColor_kBBColor_Evergreen_Black, "kBBColor_Evergreen_Black" },
	{ kStyleColor_kBBColor_Evergreen_Red, "kBBColor_Evergreen_Red" },
	{ kStyleColor_kBBColor_Evergreen_Green, "kBBColor_Evergreen_Green" },
	{ kStyleColor_kBBColor_Evergreen_Yellow, "kBBColor_Evergreen_Yellow" },
	{ kStyleColor_kBBColor_Evergreen_Blue, "kBBColor_Evergreen_Blue" },
	{ kStyleColor_kBBColor_Evergreen_Cyan, "kBBColor_Evergreen_Cyan" },
	{ kStyleColor_kBBColor_Evergreen_Pink, "kBBColor_Evergreen_Pink" },
	{ kStyleColor_kBBColor_Evergreen_White, "kBBColor_Evergreen_White" },
	{ kStyleColor_kBBColor_Evergreen_LightBlue, "kBBColor_Evergreen_LightBlue" },
	{ kStyleColor_kBBColor_Evergreen_Orange, "kBBColor_Evergreen_Orange" },
	{ kStyleColor_kBBColor_Evergreen_LightBlueAlt, "kBBColor_Evergreen_LightBlueAlt" },
	{ kStyleColor_kBBColor_Evergreen_OrangeAlt, "kBBColor_Evergreen_OrangeAlt" },
	{ kStyleColor_kBBColor_Evergreen_MediumBlue, "kBBColor_Evergreen_MediumBlue" },
	{ kStyleColor_kBBColor_Evergreen_Amber, "kBBColor_Evergreen_Amber" },
	{ kStyleColor_kBBColor_UE4_Black, "kBBColor_UE4_Black" },
	{ kStyleColor_kBBColor_UE4_DarkRed, "kBBColor_UE4_DarkRed" },
	{ kStyleColor_kBBColor_UE4_DarkGreen, "kBBColor_UE4_DarkGreen" },
	{ kStyleColor_kBBColor_UE4_DarkBlue, "kBBColor_UE4_DarkBlue" },
	{ kStyleColor_kBBColor_UE4_DarkYellow, "kBBColor_UE4_DarkYellow" },
	{ kStyleColor_kBBColor_UE4_DarkCyan, "kBBColor_UE4_DarkCyan" },
	{ kStyleColor_kBBColor_UE4_DarkPurple, "kBBColor_UE4_DarkPurple" },
	{ kStyleColor_kBBColor_UE4_DarkWhite, "kBBColor_UE4_DarkWhite" },
	{ kStyleColor_kBBColor_UE4_Red, "kBBColor_UE4_Red" },
	{ kStyleColor_kBBColor_UE4_Green, "kBBColor_UE4_Green" },
	{ kStyleColor_kBBColor_UE4_Blue, "kBBColor_UE4_Blue" },
	{ kStyleColor_kBBColor_UE4_Yellow, "kBBColor_UE4_Yellow" },
	{ kStyleColor_kBBColor_UE4_Cyan, "kBBColor_UE4_Cyan" },
	{ kStyleColor_kBBColor_UE4_Purple, "kBBColor_UE4_Purple" },
	{ kStyleColor_kBBColor_UE4_White, "kBBColor_UE4_White" },
	{ kStyleColor_ActiveSession, "ActiveSession" },
	{ kStyleColor_InactiveSession, "InactiveSession" },
	{ kStyleColor_LogLevel_VeryVerbose, "LogLevel_VeryVerbose" },
	{ kStyleColor_LogLevel_Verbose, "LogLevel_Verbose" },
	{ kStyleColor_LogLevel_Log, "LogLevel_Log" },
	{ kStyleColor_LogLevel_Display, "LogLevel_Display" },
	{ kStyleColor_LogLevel_Warning, "LogLevel_Warning" },
	{ kStyleColor_LogLevel_Error, "LogLevel_Error" },
	{ kStyleColor_LogLevel_Fatal, "LogLevel_Fatal" },
	{ kStyleColor_Multiline, "Multiline" },
	{ kStyleColor_LogBackground_Normal, "LogBackground_Normal" },
	{ kStyleColor_LogBackground_NormalAlternate0, "LogBackground_NormalAlternate0" },
	{ kStyleColor_LogBackground_NormalAlternate1, "LogBackground_NormalAlternate1" },
	{ kStyleColor_LogBackground_Bookmarked, "LogBackground_Bookmarked" },
	{ kStyleColor_LogBackground_BookmarkedAlternate0, "LogBackground_BookmarkedAlternate0" },
	{ kStyleColor_LogBackground_BookmarkedAlternate1, "LogBackground_BookmarkedAlternate1" },
	{ kStyleColor_ResizeNormal, "ResizeNormal" },
	{ kStyleColor_ResizeHovered, "ResizeHovered" },
	{ kStyleColor_ResizeActive, "ResizeActive" },
};
BB_CTASSERT(BB_ARRAYSIZE(s_styleColorNames) == kStyleColor_Count);

styleColor_e Style_FindColor(const char *name)
{
	for(u32 i = 0; i < BB_ARRAYSIZE(s_styleColorNames); ++i) {
		if(!_stricmp(s_styleColorNames[i].name, name)) {
			return (styleColor_e)s_styleColorNames[i].index;
		}
	}
	return kStyleColor_Count;
}

static void Style_AssignColors(styleConfig *config)
{
	for(u32 i = 0; i < config->colors.count; ++i) {
		styleColor *c = config->colors.data + i;
		styleColor_e idx = Style_FindColor(sb_get(&c->name));
		if(idx != kStyleColor_Count) {
			g_styleConfig.colors[idx] = styleColor_clone(c);
		}
	}
}

b32 Style_ReadConfig(const char *colorscheme)
{
	Style_ResetConfig();

	b32 ret = false;
	sb_t path = {0};
	sb_va(&path, "%s\\bb_style_%s.json", cmdline_get_exe_dir(), colorscheme);
	for(u32 i = 0; i < path.count; ++i) {
		if(path.data[i] == ' ') {
			path.data[i] = '_';
		}
	}

	JSON_Value *val = json_parse_file(sb_get(&path));
	if(val) {
		styleConfig sc = json_deserialize_styleConfig(val);
		Style_AssignColors(&sc);
		styleConfig_reset(&sc);
		json_value_free(val);
		ret = true;
	}

	sb_reset(&path);
	return ret;
}

void Style_ResetConfig(void)
{
	resolvedStyle_reset(&g_styleConfig);
}
