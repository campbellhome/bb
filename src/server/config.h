// Copyright (c) 2012-2019 Matt Campbell
// MIT license (see License.txt)

#pragma once

#if defined(__cplusplus)
extern "C" {
#endif

#include "bb.h"
#include "sb.h"

// define the Windows structs for preproc
#if 0
AUTOJSON typedef struct tagPOINT {
	LONG x;
	LONG y;
} POINT;

AUTOJSON typedef struct tagRECT {
	LONG left;
	LONG top;
	LONG right;
	LONG bottom;
} RECT;

AUTOJSON typedef struct tagWINDOWPLACEMENT {
	UINT length;
	UINT flags;
	UINT showCmd;
	POINT ptMinPosition;
	POINT ptMaxPosition;
	RECT rcNormalPosition;
} WINDOWPLACEMENT;
#endif

AUTOJSON typedef struct configWhitelistEntry_s {
	b32 allow;
	b32 autodetectedDevkit;
	u32 delay;
	u8 pad[4];
	sb_t addressPlusMask;
	sb_t applicationName;
	sb_t comment;
} configWhitelistEntry_t;
AUTOJSON typedef struct configWhitelist_s {
	u32 count;
	u32 allocated;
	configWhitelistEntry_t *data;
} configWhitelist_t;

AUTOJSON typedef struct openTargetEntry_s {
	sb_t displayName;
	sb_t commandLine;
} openTargetEntry_t;
AUTOJSON typedef struct openTargetList_s {
	u32 count;
	u32 allocated;
	openTargetEntry_t *data;
} openTargetList_t;

AUTOJSON typedef struct pathFixupEntry_s {
	sb_t src;
	sb_t dst;
} pathFixupEntry_t;
AUTOJSON typedef struct pathFixupList_s {
	u32 count;
	u32 allocated;
	pathFixupEntry_t *data;
} pathFixupList_t;

AUTOJSON typedef struct fontConfig_s {
	b32 enabled;
	u32 size;
	sb_t path;
} fontConfig_t;

AUTOJSON AUTODEFAULT(kConfigColors_Full) typedef enum tag_configColorUsage {
	kConfigColors_Full,
	kConfigColors_BgAsFg,
	kConfigColors_NoBg,
	kConfigColors_None,
	kConfigColors_Count
} configColorUsage;

AUTOJSON typedef struct tag_tooltipConfig {
	b32 enabled;
	b32 overText;
	b32 overMisc;
	b32 onlyOverSelected;
	float delay;
	u8 pad[4];
} tooltipConfig;

AUTOJSON typedef struct config_s {
	configWhitelist_t whitelist;
	openTargetList_t openTargets;
	pathFixupList_t pathFixups;
	fontConfig_t logFontConfig;
	fontConfig_t uiFontConfig;
	sb_t colorscheme;
	WINDOWPLACEMENT wp;
	u32 version;
	b32 autoTileViews;
	b32 alternateRowBackground;
	b32 textShadows;
	configColorUsage logColorUsage;
	tooltipConfig tooltips;
	b32 recordingsOpen;
	b32 singleInstanceCheck;
	b32 singleInstancePrompt;
	b32 dpiAware;
	u32 autoDeleteAfterDays;
	b32 autoCloseAll;
	b32 updateManagement;
	float doubleClickSeconds;
	float dpiScale;
	b32 updateWaitForDebugger;
	b32 updatePauseAfterSuccessfulUpdate;
	b32 updatePauseAfterFailedUpdate;
	b32 assertMessageBox;
	u8 pad[4];
} config_t;

enum { kConfigVersion = 5 };

extern config_t g_config;

b32 config_read(config_t *config);
b32 config_write(config_t *config);
config_t config_clone(config_t *config);
void config_reset(config_t *config);
void config_free(config_t *config);
void config_push_whitelist(configWhitelist_t *whitelist);
void whitelist_move_entry(configWhitelist_t *whitelist, u32 indexA, u32 indexB);
void config_validate_whitelist(configWhitelist_t *whitelist);
void open_target_move_entry(openTargetList_t *openTargets, u32 indexA, u32 indexB);
void config_validate_open_targets(openTargetList_t *openTargets);
void path_fixup_move_entry(pathFixupList_t *pathFixups, u32 indexA, u32 indexB);
void config_getwindowplacement(HWND hwnd);

#if defined(__cplusplus)
}
#endif
