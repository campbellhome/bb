// Copyright (c) 2012-2019 Matt Campbell
// MIT license (see License.txt)

#pragma once

#include "common.h"

#if defined(__cplusplus)
#include "wrap_imgui.h"
extern "C" {
#endif
extern b32 App_Init(const char *commandLine);
extern void App_Shutdown(void);
extern void App_Update(void);
extern b32 App_IsShuttingDown(void);
extern b32 App_HasFocus(void);
extern void App_RequestRender(void);
extern b32 App_GetAndClearRequestRender(void);
extern void App_RequestShutDown(void);
#include "bb_connection.h"
#include "bb_discovery_client.h"
#include "bb_discovery_server.h"
#include "bb_log.h"
#include "bb_packet.h"
#include "bb_sockets.h"
#include "bb_string.h"
#include "bb_thread.h"
#include "bb_time.h"

typedef struct globals_s {
	WNDCLASSEX wc;
	HWND hwnd;
	char viewerPath[kBBSize_MaxPath];
	char viewerName[kBBSize_MaxPath];
	b32 viewer;
	u8 pad[4];
} globals_t;

extern globals_t globals;

#if defined(__cplusplus)
}
#endif
