// Copyright (c) 2012-2019 Matt Campbell
// MIT license (see License.txt)

#pragma once

#if defined(__cplusplus)
void Fonts_MarkAtlasForRebuild();
bool Fonts_UpdateAtlas();
void Fonts_Menu();

extern "C" {
#endif
void Fonts_CacheGlyphs(const char *text);
#if defined(__cplusplus)
}
#endif
