// Copyright (c) 2012-2019 Matt Campbell
// MIT license (see License.txt)

#include "app.h"
#include "bb.h"
#include "bb_array.h"
#include "bb_file.h"
#include "bb_wrap_stdio.h"
#include "bug_reporter.h"
#include "callstack_utils.h"
#include "cmdline.h"
#include "config.h"
#include "devkit_autodetect.h"
#include "exception_handler.h"
#include "fonts.h"
#include "imgui_themes.h"
#include "imgui_utils.h"
#include "message_box.h"
#include "message_queue.h"
#include "process_utils.h"
#include "recorded_session.h"
#include "recordings.h"
#include "site_config.h"
#include "tasks.h"
#include "theme_config.h"
#include "ui_config.h"
#include "ui_message_box.h"
#include "ui_recordings.h"
#include "ui_view.h"
#include "update.h"
#include "uuid_config.h"
#include "uuid_rfc4122/uuid.h"
#include "va.h"
#include "view.h"
#include "win32_resource.h"
#include "wrap_shellscalingapi.h"

globals_t globals;
bool g_shuttingDown;
bool g_failedDiscoveryStartup;
int g_failedDiscoveryCount;
char g_bbPath[kBBSize_MaxPath] = "";

extern "C" {
int discovery_thread_init(void);
void discovery_thread_shutdown(void);
void get_appdata_folder(char *buffer, size_t bufferSize);
bool mkdir_recursive(const char *path);
}

void App_DispatchToUIMessageQueue();
static void App_GenerateColorTestLogs();

static bool s_showDemo;
static char s_imguiPath[1024];
static bool app_get_imgui_path(char *buffer, size_t bufferSize)
{
	get_appdata_folder(buffer, bufferSize);
	size_t dirLen = strlen(buffer);

	if(bb_snprintf(buffer + dirLen, bufferSize - dirLen, "/imgui.ini") < 0) {
		buffer[bufferSize - 1] = '\0';
	}

	return true;
}

bool App_InitViewer(const char *cmdline)
{
	if(cmdline && *cmdline) {
		if(*cmdline == '\"') {
			++cmdline;
		}
		size_t pathLen = bb_strncpy(globals.viewerPath, cmdline, sizeof(globals.viewerPath));
		if(pathLen) {
			if(globals.viewerPath[pathLen - 1] == '\"') {
				globals.viewerPath[pathLen - 1] = '\0';
			}
			FILE *fp = fopen(globals.viewerPath, "rb");
			if(fp) {
				fclose(fp);

				const char *srcName = strrchr(globals.viewerPath, '\\');
				if(srcName) {
					++srcName;
				} else {
					srcName = globals.viewerPath;
				}
				const char *closeBrace = strrchr(srcName, '}');
				if(closeBrace) {
					srcName = closeBrace + 1;
				}

				bb_strncpy(globals.viewerName, srcName, sizeof(globals.viewerName));
				char *ext = strrchr(globals.viewerName, '.');
				if(ext) {
					*ext = '\0';
				}

				return true;
			}
		}
	}
	return false;
}

HRESULT SetProcessDpiAwarenessShim(_In_ PROCESS_DPI_AWARENESS value)
{
	HMODULE hModule = GetModuleHandleA("shcore.dll");
	if(hModule) {
		typedef HRESULT(WINAPI * Proc)(_In_ PROCESS_DPI_AWARENESS value);
		Proc proc = (Proc)(void *)(GetProcAddress(hModule, "SetProcessDpiAwareness"));
		if(proc) {
			return proc(value);
		}
	}
	return STG_E_UNIMPLEMENTEDFUNCTION;
}

static bool App_CreateWindow(void)
{
	extern LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

	if(g_config.dpiAware) {
		SetProcessDpiAwarenessShim(PROCESS_PER_MONITOR_DPI_AWARE);
	}

	const char *classname = (globals.viewer) ? "BlackboxViewer" : "BlackboxHost";

	if(!globals.viewer) {
		if(g_config.singleInstanceCheck) {
			HWND hExisting = FindWindowA(classname, nullptr);
			if(hExisting) {
				int response = (g_config.singleInstancePrompt)
				                   ? MessageBoxA(nullptr, "Blackbox is already running - open existing window?", "Blackbox is already running", MB_YESNO)
				                   : IDYES;
				if(response == IDYES) {
					WINDOWINFO info = {};
					info.cbSize = sizeof(WINDOWINFO);
					if(!GetWindowInfo(hExisting, &info) || info.rcClient.left == info.rcClient.right) {
						ShowWindow(hExisting, SW_RESTORE);
					}
					SetForegroundWindow(hExisting);
					return false;
				}
			}
		}
	}

	WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, WndProc, 0L, 0L, GetModuleHandle(NULL), LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_MAINICON)), LoadCursor(NULL, IDC_ARROW), NULL, NULL, classname, NULL };
	globals.wc = wc;
	RegisterClassEx(&globals.wc);

	WINDOWPLACEMENT wp = g_config.wp;
	const char *title = (globals.viewer) ? va("%s.bbox - Blackbox", globals.viewerName) : "Blackbox";
	globals.hwnd = CreateWindow(classname, title, WS_OVERLAPPEDWINDOW, 100, 100, 1280, 800, NULL, NULL, globals.wc.hInstance, NULL);
	if(wp.rcNormalPosition.right > wp.rcNormalPosition.left) {
		if(wp.showCmd == SW_SHOWMINIMIZED) {
			wp.showCmd = SW_SHOWNORMAL;
		}
		SetWindowPlacement(globals.hwnd, &wp);
	}
	BB_LOG("Startup", "hwnd: %p", globals.hwnd);
	return globals.hwnd != 0;
}

static void App_WriteRuntimeXml(const bugReport *report)
{
	sb_t contents = { BB_EMPTY_INITIALIZER };

	sb_append(&contents, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	sb_append(&contents, "<FGenericCrashContext>\n");
	sb_append(&contents, "<RuntimeProperties>\n");
	sb_va(&contents, "<CrashGUID>%s</CrashGUID>\n", sb_get(&report->guid));
	switch(report->type) {
	case kBugType_Bug:
		sb_append(&contents, "<CrashType>Bug</CrashType>\n");
		break;
	case kBugType_Assert:
		sb_append(&contents, "<CrashType>Assert</CrashType>\n");
		break;
	case kBugType_Crash:
		sb_append(&contents, "<CrashType>Crash</CrashType>\n");
		break;
	default:
		BB_BREAK();
	}
	sb_append(&contents, "<GameName>Blackbox</GameName>\n");
	sb_append(&contents, "<ExecutableName>bb</ExecutableName>\n");
	sb_va(&contents, "<EngineVersion>%s</EngineVersion>\n", Update_GetCurrentVersion());
	sb_va(&contents, "<BuildVersion>%s</BuildVersion>\n", Update_GetCurrentVersion());
	sb_va(&contents, "<CommandLine>%s</CommandLine>\n", cmdline_get_full());
	sb_append(&contents, "<Misc.OSVersionMajor>Windows</Misc.OSVersionMajor>\n");
	sb_append(&contents, "<Misc.OSVersionMinor></Misc.OSVersionMinor>\n");
	sb_append(&contents, "</RuntimeProperties>\n");
	sb_append(&contents, "</FGenericCrashContext>\n");

	if(contents.count > 1) {
		sb_t path = { BB_EMPTY_INITIALIZER };
		sb_va(&path, "%s\\CrashContext.runtime-xml", sb_get(&report->dir));
		bb_file_handle_t fp = bb_file_open_for_write(sb_get(&path));
		if(fp) {
			bb_file_write(fp, contents.data, contents.count - 1);
			bb_file_close(fp);
		}
		sb_reset(&path);
	}
	sb_reset(&contents);
}

extern "C" static bbassert_action_e App_AssertHandler(const char *condition, const char *message, const char *file, const int line)
{
	// static buffers so we can safely assert in a memory allocator :)
	static bb_thread_local char output[2048];
	int len;
	BB_WARNING_PUSH(4996);
	if(message && *message) {
		len = bb_snprintf(output, sizeof(output), "Assert Failure: '%s': %s [%s:%d]", condition, message, file, line);
	} else {
		len = bb_snprintf(output, sizeof(output), "Assert Failure: '%s' [%s:%d]", condition, file, line);
	}
	len = (len > 0 && len < (int)sizeof(output)) ? len : (int)sizeof(output) - 1;
	output[len] = '\0';
	BB_WARNING_POP;

	sb_t callstack = callstack_generate_sb(2); // skip App_AssertHandler and bbassert_dispatch
	if(message && *message) {
		BB_ERROR("Assert", "Assert Failure: '%s': %s\n\n%s", condition, message, sb_get(&callstack));
	} else {
		BB_ERROR("Assert", "Assert Failure: '%s'\n\n%s", condition, sb_get(&callstack));
	}

	bbassert_action_e action = kBBAssertAction_Break;
	if(sb_len(&g_site_config.bugProject)) {
		bugReport *report = bug_report_init();
		if(report) {
			report->type = kBugType_Assert;
			report->bSilent = true;
			sb_append(&report->title, output);
			sb_t target = { BB_EMPTY_INITIALIZER };
			if(g_bbPath[0]) {
				BB_FLUSH();
				sb_va(&target, "%s\\bb.bbox", sb_get(&report->dir));
				CopyFileA(g_bbPath, sb_get(&target), false);
				sb_reset(&target);
			}
			if(callstack.count > 1) {
				sb_va(&target, "%s\\Callstack.txt", sb_get(&report->dir));
				bb_file_handle_t fp = bb_file_open_for_write(sb_get(&target));
				if(fp) {
					bb_file_write(fp, callstack.data, callstack.count - 1);
					bb_file_close(fp);
				}
				sb_reset(&target);
			}
			App_WriteRuntimeXml(report);
			bug_report_dispatch_async(report);
		}
	}
	BB_TRACE(kBBLogLevel_Verbose, "Assert", "Finished with assert handler");
	BB_FLUSH();
	if(g_config.assertMessageBox) {
		const char *buttonDesc = "\nBreak in the debugger?";
		if(message && *message) {
			len = bb_snprintf(output, sizeof(output), "%s(%d):\n\nAssert Failure: '%s': %s\n%s\n\n%s", file, line, condition, message, buttonDesc, sb_get(&callstack));
		} else {
			len = bb_snprintf(output, sizeof(output), "%s(%d):\n\nAssert Failure: '%s'\n%s\n\n%s", file, line, condition, buttonDesc, sb_get(&callstack));
		}
		len = (len > 0 && len < (int)sizeof(output)) ? len : (int)sizeof(output) - 1;
		output[len] = '\0';

		if(MessageBoxA(globals.hwnd, output, "Blackbox Assert Failure", MB_YESNO) == IDYES) {
			action = kBBAssertAction_Break;
		} else {
			action = kBBAssertAction_Continue;
		}
	}
	sb_reset(&callstack);
	return action;
}

static sb_t App_BuildExceptionInfo(EXCEPTION_POINTERS *pExPtrs)
{
	sb_t out = { BB_EMPTY_INITIALIZER };
	sb_append(&out, "Blackbox Unhandled Exception: ");
	EXCEPTION_RECORD *record = pExPtrs->ExceptionRecord;
	switch(record->ExceptionCode) {
	case EXCEPTION_ACCESS_VIOLATION: {
		sb_append(&out, "EXCEPTION_ACCESS_VIOLATION ");
		if(record->ExceptionInformation[0]) {
			sb_append(&out, "writing");
		} else {
			sb_append(&out, "reading");
		}
		sb_va(&out, " address 0x%08X", record->ExceptionInformation[1]);
		break;
	}
	case EXCEPTION_ARRAY_BOUNDS_EXCEEDED: sb_append(&out, "EXCEPTION_ARRAY_BOUNDS_EXCEEDED"); break;
	case EXCEPTION_BREAKPOINT: sb_append(&out, "EXCEPTION_BREAKPOINT"); break;
	case EXCEPTION_DATATYPE_MISALIGNMENT: sb_append(&out, "EXCEPTION_DATATYPE_MISALIGNMENT"); break;
	case EXCEPTION_FLT_DENORMAL_OPERAND: sb_append(&out, "EXCEPTION_FLT_DENORMAL_OPERAND"); break;
	case EXCEPTION_FLT_DIVIDE_BY_ZERO: sb_append(&out, "EXCEPTION_FLT_DIVIDE_BY_ZERO"); break;
	case EXCEPTION_FLT_INEXACT_RESULT: sb_append(&out, "EXCEPTION_FLT_INEXACT_RESULT"); break;
	case EXCEPTION_FLT_INVALID_OPERATION: sb_append(&out, "EXCEPTION_FLT_INVALID_OPERATION"); break;
	case EXCEPTION_FLT_OVERFLOW: sb_append(&out, "EXCEPTION_FLT_OVERFLOW"); break;
	case EXCEPTION_FLT_STACK_CHECK: sb_append(&out, "EXCEPTION_FLT_STACK_CHECK"); break;
	case EXCEPTION_FLT_UNDERFLOW: sb_append(&out, "EXCEPTION_FLT_UNDERFLOW"); break;
	case EXCEPTION_ILLEGAL_INSTRUCTION: sb_append(&out, "EXCEPTION_ILLEGAL_INSTRUCTION"); break;
	case EXCEPTION_IN_PAGE_ERROR: sb_append(&out, "EXCEPTION_IN_PAGE_ERROR"); break;
	case EXCEPTION_INT_DIVIDE_BY_ZERO: sb_append(&out, "EXCEPTION_INT_DIVIDE_BY_ZERO"); break;
	case EXCEPTION_INT_OVERFLOW: sb_append(&out, "EXCEPTION_INT_OVERFLOW"); break;
	case EXCEPTION_INVALID_DISPOSITION: sb_append(&out, "EXCEPTION_INVALID_DISPOSITION"); break;
	case EXCEPTION_NONCONTINUABLE_EXCEPTION: sb_append(&out, "EXCEPTION_NONCONTINUABLE_EXCEPTION"); break;
	case EXCEPTION_PRIV_INSTRUCTION: sb_append(&out, "EXCEPTION_PRIV_INSTRUCTION"); break;
	case EXCEPTION_SINGLE_STEP: sb_append(&out, "EXCEPTION_SINGLE_STEP"); break;
	case EXCEPTION_STACK_OVERFLOW: sb_append(&out, "EXCEPTION_STACK_OVERFLOW"); break;
	default:
		sb_va(&out, "0x%8.8X", record->ExceptionCode);
		break;
	}
	return out;
}

extern "C" static void App_ExceptionHandler(EXCEPTION_POINTERS *pExPtrs)
{
	//volatile bool s_done = false;
	//while(!s_done) {
	//	bb_sleep_ms(1000);
	//}

	sb_t title = App_BuildExceptionInfo(pExPtrs);
	BB_ERROR("Crash", "%s", sb_get(&title));
	sb_t callstack = callstack_generate_crash_sb();
	BB_ERROR("Crash", "%s", sb_get(&callstack));

	bugReport *report = bug_report_init();
	if(report) {
		report->type = kBugType_Crash;
		report->bSilent = false;
		report->title = sb_clone(&title);
		sb_t target = { BB_EMPTY_INITIALIZER };
		if(g_bbPath[0]) {
			BB_FLUSH();
			sb_va(&target, "%s\\bb.bbox", sb_get(&report->dir));
			CopyFileA(g_bbPath, sb_get(&target), false);
			sb_reset(&target);
		}
		if(callstack.count > 1) {
			sb_va(&target, "%s\\Callstack.txt", sb_get(&report->dir));
			bb_file_handle_t fp = bb_file_open_for_write(sb_get(&target));
			if(fp) {
				bb_file_write(fp, callstack.data, callstack.count - 1);
				bb_file_close(fp);
			}
			sb_reset(&target);
		}
		App_WriteRuntimeXml(report);
		bug_report_dispatch_sync(report);
	} else {
		MessageBoxA(0, va("%s\n\n%s", sb_get(&title), sb_get(&callstack)), "Blackbox Unhandled Exception", MB_OK);
	}
	sb_reset(&callstack);
	sb_reset(&title);
}

void App_DebugCallstack()
{
	BB_LOG("UI::Menu::Debug", "User-initiated callstack log");
	sb_t callstack = callstack_generate_sb(0);
	BB_LOG("Debug::Callstack", "%s", sb_get(&callstack));
	sb_reset(&callstack);
}

void App_DebugAssert()
{
	BB_LOG("UI::Menu::Debug", "User-initiated assert");
	BB_ASSERT(false);
}
void App_DebugCrash()
{
	BB_LOG("UI::Menu::Debug", "User-initiated access violation");
	char *badAddress = (char *)1;
	*badAddress = 0;
}
static int infinite_recursion(int i)
{
	if(i < 0)
		return 0;
	return i + infinite_recursion(i + 1);
}
void App_DebugInfiniteRecursion()
{
	BB_LOG("UI::Menu::Debug", "User-initiated infinite recursion");
	infinite_recursion(0);
}

extern "C" b32 App_Init(const char *cmdline)
{
	callstack_init();
	cmdline_init_composite(cmdline);
	site_config_init();
	uuid_init(&uuid_read_state, &uuid_write_state);
	if(sb_len(&g_site_config.bugProject)) {
		bug_reporter_init(sb_get(&g_site_config.bugProject), sb_get(&g_site_config.bugAssignee));
	}
	install_unhandled_exception_handler(&App_ExceptionHandler);
	app_get_imgui_path(s_imguiPath, sizeof(s_imguiPath));
	ImGuiIO &io = ImGui::GetIO();
	io.IniFilename = s_imguiPath;

	globals.viewer = App_InitViewer(cmdline);

	const char *applicationName = globals.viewer ? u8"Blackbox Viewer" : u8"Blackbox";
	const char *applicationFilename = "bb";
	get_appdata_folder(g_bbPath, sizeof(g_bbPath));
	size_t dirLen = strlen(g_bbPath);
	if(dirLen) {
		if(bb_snprintf(g_bbPath + dirLen, sizeof(g_bbPath) - dirLen, "\\%s", applicationFilename) < 0) {
			g_bbPath[sizeof(g_bbPath) - 1] = '\0';
		}
		if(mkdir_recursive(g_bbPath)) {
			dirLen = strlen(g_bbPath);
			rfc_uuid uuid;
			char uuidBuffer[64];
			uuid_create(&uuid);
			format_uuid(&uuid, uuidBuffer, sizeof(uuidBuffer));
			if(bb_snprintf(g_bbPath + dirLen, sizeof(g_bbPath) - dirLen, "\\{%s}%s.bbox", uuidBuffer, applicationFilename) < 0) {
				g_bbPath[sizeof(g_bbPath) - 1] = '\0';
			}
			bb_init_file(g_bbPath);
		}
	}

	BB_INIT_WITH_FLAGS(applicationName, kBBInitFlag_NoOpenView);
	BB_THREAD_SET_NAME("main");
	BB_LOG("Startup", "%s starting...\nPath: %s\n", applicationName, g_bbPath);
	BB_LOG("Startup", "Arguments: %s", cmdline);
	bbthread_set_name("main");
	bbassert_set_handler(&App_AssertHandler);

	if(!Update_Init()) {
		return false;
	}

	process_init();
	tasks_startup();
	mq_init();
	config_read(&g_config);
	config_validate_whitelist(&g_config.whitelist);
	config_validate_open_targets(&g_config.openTargets);
	Style_Init();
	if(globals.viewer) {
		new_recording_t cmdlineRecording;
		GetSystemTimeAsFileTime(&cmdlineRecording.filetime);
		cmdlineRecording.applicationName = globals.viewerName;
		cmdlineRecording.applicationFilename = globals.viewerName;
		cmdlineRecording.path = globals.viewerPath;
		cmdlineRecording.openView = true;
		cmdlineRecording.mainLog = false;
		cmdlineRecording.mqId = mq_invalid_id();
		cmdlineRecording.platform = kBBPlatform_Unknown;
		to_ui(kToUI_RecordingStart, "%s", recording_build_start_identifier(cmdlineRecording));

		g_config.recordingsOpen = false;
		g_config.autoTileViews = 1;
		g_config.autoDeleteAfterDays = 0;
		return App_CreateWindow();
	} else if(bbnet_init()) {
		if(discovery_thread_init() != 0) {
			new_recording_t recording;
			config_push_whitelist(&g_config.whitelist);
			GetSystemTimeAsFileTime(&recording.filetime);
			recording.applicationName = applicationName;
			recording.applicationFilename = applicationFilename;
			recording.path = g_bbPath;
#ifdef _DEBUG
			recording.openView = true;
#else
			recording.openView = false;
#endif
			recording.mainLog = true;
			recording.mqId = mq_invalid_id();
			recording.platform = bb_platform();
			to_ui(kToUI_RecordingStart, "%s", recording_build_start_identifier(recording));

			recordings_init();
			App_GenerateColorTestLogs();
			return App_CreateWindow();
		}
	}
	return false;
}

void App_Shutdown()
{
	mq_pre_shutdown();
	Update_Shutdown();
	Style_ResetConfig();
	mb_shutdown();
	tasks_shutdown();
	devkit_autodetect_shutdown();
	UIConfig_Reset();
	recordings_shutdown();
	discovery_thread_shutdown();
	while(recorded_session_t *session = recorded_session_get(0)) {
		while(session->views.count) {
			u32 viewIndex = session->views.count - 1;
			view_t *view = session->views.data + viewIndex;
			view_reset(view);
			bba_erase(session->views, viewIndex);
		}
		recorded_session_close(session);
	}
	if(!globals.viewer && g_config.version != 0) {
		config_write(&g_config);
	}
	config_reset(&g_config);
	site_config_shutdown();
	bbnet_shutdown();
	message_queue_message_t message;
	while(mq_consume_to_ui(&message)) {
		bb_log("(shutdown) to_ui type:%d msg:%s", message.command, message.text);
	}
	mq_shutdown();
	bug_reporter_shutdown();
	uuid_shutdown();
	BB_SHUTDOWN();

	if(globals.hwnd) {
		DestroyWindow(globals.hwnd);
	}
	if(globals.wc.hInstance) {
		UnregisterClass(globals.wc.lpszClassName, globals.wc.hInstance);
	}
	cmdline_shutdown();
	callstack_shutdown();
}

int g_appRequestRenderCount;
extern "C" void App_RequestRender(void)
{
	g_appRequestRenderCount = 3;
}
extern "C" b32 App_GetAndClearRequestRender(void)
{
	b32 ret = g_appRequestRenderCount > 0;
	g_appRequestRenderCount = BB_MAX(0, g_appRequestRenderCount - 1);
	return ret;
}
extern "C" void App_RequestShutDown(void)
{
	g_shuttingDown = true;
}

static bb_thread_return_t app_test_log_thread(void *args)
{
	u32 i = (u32)(u64)args;
	BB_THREAD_SET_NAME(va("app_test_log_thread %u", i));

	u64 count = 0;
	u64 start = bb_current_time_ms();
	u64 now = start;
	u64 end = now + 10000;
	while(now < end) {
		BB_TRACE(kBBLogLevel_Verbose, "Test::ThreadSpam", "app_test_log_thread %u %llu dt %llu ms", i, ++count, now - start);
		now = bb_current_time_ms();
	}

	BB_THREAD_END();
	return 0;
}

static void App_GenerateLineTestLogs()
{
	BB_LOG("Test::Multiline", "FirstLine\nSecondLine\n");
	BB_LOG("Test::Multiline", "FirstWithTrailingBlank\n\n");
	BB_LOG("Test::Multiline", "\n\n");
	BB_LOG("Test::Multiline", "\n\nPrevious was 2 blank lines - this has 2 before and 3 after.\n\n\n");

	BB_LOG("Test::Long Line", "This is a reasonably long line, which one might expect to wrap in a tooltip, even though it does not in the log view.  It *should* wrap at 600px in the tooltip.  At least, it did when this was written.  That might have changed by now, but the point remains - this is a long log line.\n");

	BB_LOG_PARTIAL("Test::Partial", "this is a ");
	BB_LOG_PARTIAL("Test::Partial", "^0__partial__^7");
	BB_LOG_PARTIAL("Test::Partial", " test\n");

	BB_LOG_PARTIAL("Test::Partial", "this is a ");
	BB_LOG_PARTIAL("Test::Partial", "__partial__");
	BB_LOG_PARTIAL("Test::Partial::Subcategory", "(interrupting partial different category)");
	BB_LOG_PARTIAL("Test::Partial", " test\n");

	BB_LOG_PARTIAL("Test::Partial", "this is a ");
	BB_LOG_PARTIAL("Test::Partial", "__partial__");
	BB_WARNING_PARTIAL("Test::Partial", "(interrupting partial warning)");
	BB_LOG_PARTIAL("Test::Partial", " test\n");

	BB_LOG_PARTIAL("Test::Partial", "this is a ");
	BB_LOG_PARTIAL("Test::Partial", "__partial__");
	BB_LOG("Test::Partial", "(interrupting log)");
	BB_LOG_PARTIAL("Test::Partial", " test\n");

	BB_LOG("Test::Multiline", "^8Line terminated by \\r\\n\r\nLine terminated by \\n\nLine terminated by \\r\rLine unterminated");
}

static void App_GeneratePIEInstanceTestLogs()
{
	BB_TRACE_DYNAMIC_PREFORMATTED(__FILE__, (u32)__LINE__, kBBLogLevel_Log, 0, "Test::PIEInstance", "This log is from PIEInstance 0");
	BB_TRACE_DYNAMIC_PREFORMATTED(__FILE__, (u32)__LINE__, kBBLogLevel_Log, 1, "Test::PIEInstance", "This log is from PIEInstance 1");
	BB_TRACE_DYNAMIC_PREFORMATTED(__FILE__, (u32)__LINE__, kBBLogLevel_Log, 2, "Test::PIEInstance", "This log is from PIEInstance 2");
	BB_TRACE_DYNAMIC_PREFORMATTED(__FILE__, (u32)__LINE__, kBBLogLevel_Log, 3, "Test::PIEInstance", "This log is from PIEInstance 3");
	BB_TRACE_DYNAMIC_PREFORMATTED(__FILE__, (u32)__LINE__, kBBLogLevel_Log, 4, "Test::PIEInstance", "This log is from PIEInstance 4");
}

static void App_GenerateColorTestLogs()
{
	BB_LOG("Test::Blink", "^F"
	                      "This"
	                      "^F"
	                      " is a %s"
	                      "colored and ^Fblinking^F%s log\n",
	       warningColorStr, normalColorStr);

	BB_TRACE_DYNAMIC_PREFORMATTED(__FILE__, (u32)__LINE__, kBBLogLevel_VeryVerbose, 0, "Test::Color::VeryVerbose", "This log is VeryVerbose");
	BB_TRACE_DYNAMIC_PREFORMATTED(__FILE__, (u32)__LINE__, kBBLogLevel_Verbose, 0, "Test::Color::Verbose", "This log is Verbose");
	BB_TRACE_DYNAMIC_PREFORMATTED(__FILE__, (u32)__LINE__, kBBLogLevel_Log, 0, "Test::Color::Log", "This log is Log");
	BB_TRACE_DYNAMIC_PREFORMATTED(__FILE__, (u32)__LINE__, kBBLogLevel_Display, 0, "Test::Color::Display", "This log is Display");
	BB_TRACE_DYNAMIC_PREFORMATTED(__FILE__, (u32)__LINE__, kBBLogLevel_Warning, 0, "Test::Color::Warning", "This log is Warning");
	BB_TRACE_DYNAMIC_PREFORMATTED(__FILE__, (u32)__LINE__, kBBLogLevel_Error, 0, "Test::Color::Error", "This log is Error");
	BB_TRACE_DYNAMIC_PREFORMATTED(__FILE__, (u32)__LINE__, kBBLogLevel_Fatal, 0, "Test::Color::Fatal", "This log is Fatal");

	for(int i = 0; i < kNumColorKeys; ++i) {
		char c = kFirstColorKey + (char)i;
		BB_LOG("Test::Color::Log", "This is a log with %c%cColor %c%c%c%c%s, which should be %s\n",
		       kColorKeyPrefix, c, kColorKeyPrefix, kColorKeyPrefix, c, c, normalColorStr, textColorNames[i + kColorKeyOffset]);
	}

	for(int i = 0; i < kNumColorKeys; ++i) {
		char c = kFirstColorKey + (char)i;
		BB_WARNING("Test::Color::Warning", "This is a warning with %c%cColor %c%c%c%c%s, which should be %s\n",
		           kColorKeyPrefix, c, kColorKeyPrefix, kColorKeyPrefix, c, c, warningColorStr, textColorNames[i + kColorKeyOffset]);
	}

	for(int i = 0; i < kNumColorKeys; ++i) {
		char c = kFirstColorKey + (char)i;
		BB_ERROR("Test::Color::Error", "This is an error with %c%cColor %c%c%c%c%s, which should be %s\n",
		         kColorKeyPrefix, c, kColorKeyPrefix, kColorKeyPrefix, c, c, errorColorStr, textColorNames[i + kColorKeyOffset]);
	}

	for(int i = kBBColor_UE4_Black; i < kBBColor_Count; ++i) {
		BB_SET_COLOR((bb_color_t)i, kBBColor_Default);
		BB_LOG("Test::Color::SetColor", "This log text should be %s", textColorNames[i]);
	}
	for(int i = kBBColor_UE4_Black; i < kBBColor_Count; ++i) {
		bb_color_t fgColor = (i == kBBColor_UE4_DarkBlue ||
		                      i == kBBColor_UE4_Blue ||
		                      i == kBBColor_UE4_Black)
		                         ? kBBColor_Default
		                         : kBBColor_UE4_Black;
		BB_SET_COLOR(fgColor, (bb_color_t)i);
		BB_LOG("Test::Color::SetColor", "This log text should be on %s", textColorNames[i]);
	}
	BB_SET_COLOR(kBBColor_Default, kBBColor_Default);
}

void App_GenerateSpamTestLogs()
{
	for(u64 i = 0; i < 10; ++i) {
		bbthread_create(app_test_log_thread, (void *)i);
	}
}

void App_Update()
{
	devkit_autodetect_tick();
	tasks_tick();
	if(!globals.viewer) {
		if(ImGui::BeginMainMenuBar()) {
			if(ImGui::BeginMenu("File")) {
				if(ImGui::MenuItem("Exit")) {
					g_shuttingDown = true;
				}
				ImGui::EndMenu();
			}
			if(!UIConfig_IsOpen()) {
				if(ImGui::BeginMenu("Edit")) {
					if(ImGui::MenuItem("Recordings", NULL, &g_config.recordingsOpen)) {
						BB_LOG("UI::Menu::Recordings", "UIRecordings_ToggleOpen");
					}
					if(ImGui::MenuItem("Preferences")) {
						BB_LOG("UI::Menu::Config", "UIConfig_Open");
						UIConfig_Open(&g_config);
					}
					ImGui::EndMenu();
				}
			}
			if(ImGui::BeginMenu("Debug")) {
				if(ImGui::MenuItem("DEBUG Reload style colors")) {
					Style_Apply();
				}
				if(ImGui::BeginMenu("DEBUG Scale")) {
					void QueueUpdateDpiDependentResources();
					if(ImGui::MenuItem("1")) {
						g_config.dpiScale = 1.0f;
						QueueUpdateDpiDependentResources();
					}
					if(ImGui::MenuItem("1.25")) {
						g_config.dpiScale = 1.25f;
						QueueUpdateDpiDependentResources();
					}
					if(ImGui::MenuItem("1.5")) {
						g_config.dpiScale = 1.5f;
						QueueUpdateDpiDependentResources();
					}
					if(ImGui::MenuItem("1.75")) {
						g_config.dpiScale = 1.75f;
						QueueUpdateDpiDependentResources();
					}
					if(ImGui::MenuItem("2")) {
						g_config.dpiScale = 2.0f;
						QueueUpdateDpiDependentResources();
					}
					ImGui::EndMenu();
				}
				Fonts_Menu();
				if(ImGui::BeginMenu("Asserts and Crashes")) {
					if(ImGui::Checkbox("Assert MessageBox", &g_config.assertMessageBox)) {
						BB_LOG("UI::Menu::Debug", "Assert MessageBox: %d", g_config.assertMessageBox);
					}
					if(ImGui::MenuItem("Log Callstack")) {
						App_DebugCallstack();
					}
					if(ImGui::MenuItem("Assert")) {
						App_DebugAssert();
					}
					if(ImGui::MenuItem("Crash - access violation")) {
						App_DebugCrash();
					}
					if(ImGui::MenuItem("Crash - infinite recursion")) {
						App_DebugInfiniteRecursion();
					}
					ImGui::EndMenu();
				}
				if(ImGui::BeginMenu("DEBUG Updates")) {
					ImGui::MenuItem("Wait for debugger", nullptr, &g_config.updateWaitForDebugger);
					ImGui::MenuItem("Pause after successful update", nullptr, &g_config.updatePauseAfterSuccessfulUpdate);
					ImGui::MenuItem("Pause after failed update", nullptr, &g_config.updatePauseAfterFailedUpdate);
					ImGui::EndMenu();
				}
				ImGui::EndMenu();
			}
			if(ImGui::BeginMenu("Help")) {
				if(ImGui::MenuItem("Demo")) {
					BB_LOG("UI::Menu::Demo", "s_showDemo -> %d", !s_showDemo);
					s_showDemo = !s_showDemo;
				}
				if(ImGui::MenuItem("Logging Test - Colors")) {
					App_GenerateColorTestLogs();
				}
				if(ImGui::MenuItem("Logging Test - Lines")) {
					App_GenerateLineTestLogs();
				}
				if(ImGui::MenuItem("Logging Test - PIEInstance")) {
					App_GeneratePIEInstanceTestLogs();
				}
				if(ImGui::MenuItem("Logging Test - Thread Spam")) {
					App_GenerateSpamTestLogs();
				}
				recording_t *r = recordings_find_main_log();
				recorded_session_t *s = r ? recorded_session_find(r->path) : nullptr;
				if(s) {
					bool logReads = s->logReads != 0;
					if(ImGui::MenuItem("Log All Reads", NULL, &logReads)) {
						s->logReads = logReads;
						BB_LOG("Debug", "Toggled spammy for '%s'\n", s->appInfo.packet.appInfo.applicationName);
					}
				}
				ImGui::EndMenu();
			}
			if(ImGui::BeginMenu("Update")) {
				updateManifest_t *manifest = Update_GetManifest();
				auto AnnotateVersion = [manifest](const char *version) {
					const char *annotated = version;
					if(version && !bb_stricmp(version, sb_get(&manifest->stable))) {
						annotated = va("%s (stable)", version);
					} else if(version && !bb_stricmp(version, sb_get(&manifest->latest))) {
						annotated = va("%s (latest)", version);
					}
					return annotated;
				};
				const char *currentVersion = Update_GetCurrentVersion();
				const char *currentVersionAnnotated = AnnotateVersion(currentVersion);
				ImGui::MenuItem(va("version %s", *currentVersionAnnotated ? currentVersionAnnotated : "unknown"), nullptr, false, false);
				if(ImGui::MenuItem("Check for updates")) {
					Update_CheckForUpdates();
				}
				if(ImGui::BeginMenu("Set desired version")) {
					if(ImGui::MenuItem("stable", nullptr, Update_IsDesiredVersion("stable"))) {
						Update_SetDesiredVersion("stable");
					}
					if(ImGui::MenuItem("latest", nullptr, Update_IsDesiredVersion("latest"))) {
						Update_SetDesiredVersion("latest");
					}
					for(u32 i = 0; i < (manifest ? manifest->versions.count : 0); ++i) {
						updateVersion_t *version = manifest->versions.data + i;
						const char *versionName = sb_get(&version->name);
						if(ImGui::MenuItem(AnnotateVersion(versionName), nullptr, Update_IsDesiredVersion(versionName))) {
							Update_SetDesiredVersion(versionName);
						}
					}
					ImGui::EndMenu();
				}
				if(g_config.updateManagement && *currentVersion && !Update_IsStableVersion(currentVersion)) {
					if(ImGui::MenuItem(va("Promote %s to stable version", currentVersion))) {
						Update_SetStableVersion(currentVersion);
					}
				}
				if(g_updateIgnoredVersion) {
					if(ImGui::MenuItem(va("Update to version %u and restart", g_updateIgnoredVersion))) {
						Update_RestartAndUpdate(g_updateIgnoredVersion);
					}
				}
				ImGui::EndMenu();
			}

			ImFont *font = ImGui::GetFont();
			ImVec2 textSize = font->CalcTextSizeA(font->FontSize, FLT_MAX, 0.0f, "Recordings");

			ImGuiStyle &style = ImGui::GetStyle();
			float checkWidth = style.FramePadding.x * 4 + style.ItemInnerSpacing.x + textSize.y;

			float width = BB_MAX((textSize.x + checkWidth + 10) * g_config.dpiScale, UIRecordings_WidthWhenOpen());
			ImGui::SameLine(ImGui::GetWindowWidth() - width);
			if(ImGui::Checkbox("Recordings", &g_config.recordingsOpen)) {
				BB_LOG("UI::Menu::Recordings", "UIRecordings_ToggleOpen");
			}
			if(ImGui::BeginPopupContextItem("RecordingsContext")) {
				if(ImGui::Selectable("Auto-close all")) {
					recorded_session_auto_close_all();
				}
				ImGui::EndPopup();
			}
			ImGui::EndMainMenuBar();
		}
	}

	App_DispatchToUIMessageQueue();

	recordings_autodelete_old_recordings();

	if(s_showDemo) {
		ImGui::ShowTestWindow();
	}
	Update_Tick();
	UIConfig_Update(&g_config);
	UIRecordings_Update(g_config.autoTileViews != 0);
	UIRecordedView_UpdateAll(g_config.autoTileViews != 0);
	UIMessageBox_Update();
}

extern "C" b32 App_IsShuttingDown()
{
	return g_shuttingDown;
}

void App_DispatchToUIMessageQueue()
{
	message_queue_message_t message;
	while(mq_consume_to_ui(&message)) {
		App_RequestRender();
		switch(message.command) {
		case kToUI_AddExistingFile:
			recording_add_existing(message.text, true);
			break;
		case kToUI_AddInvalidExistingFile:
			recording_add_existing(message.text, false);
			break;
		case kToUI_RecordingStart:
			recording_started(message.text);
			break;
		case kToUI_RecordingStop:
			recording_stopped(message.text);
			break;
		case kToUI_DiscoveryStatus:
			if(!g_failedDiscoveryStartup && !strcmp(message.text, "Retrying") && !globals.viewer) {
				++g_failedDiscoveryCount;
				if(g_failedDiscoveryCount > 10) {
					g_failedDiscoveryStartup = true;
					recording_t *recording = recordings_find_main_log();
					if(recording) {
						recorded_session_t *session = recorded_session_find(recording->path);
						if(!session) {
							recorded_session_open(recording->path, recording->applicationFilename, true, recording->active != 0, recording->outgoingMqId);
						}
					}

					messageBox mb = {};
					sdict_add_raw(&mb.data, "title", "Discovery error");
					sdict_add_raw(&mb.data, "text", "Failed to start listening for incoming connections.\nSee log for details.");
					sdict_add_raw(&mb.data, "button1", "Ok");
					mb_queue(mb);
				}
			}
			break;
		default:
			bb_log("to_ui type:%d msg:%s", message.command, message.text);
			break;
		}
	}
}
