// Copyright (c) 2012-2019 Matt Campbell
// MIT license (see License.txt)

#pragma once

#include "common.h"

void UIRecordings_Open();
void UIRecordings_Close();
void UIRecordings_Update(bool autoTileViews);
float UIRecordings_Width();
float UIRecordings_WidthWhenOpen();
