@echo off

REM Build for Visual Studio compiler. Run your copy of vcvars32.bat or vcvarsall.bat to setup command-line compiler.

setlocal
cls

set PlatformToolset=v110
set Rebuild=
set Configuration=
set Analyze=

call :checkarg %1
call :checkarg %2
call :checkarg %3
call :checkarg %4
call :checkarg %5
call :checkarg %6
call :checkarg %7
call :checkarg %8
call :checkarg %9
goto dobuild

:checkarg
if "%1" == "debug" set Configuration=%1
if "%1" == "release" set Configuration=%1
if "%1" == "rebuild" set Rebuild=/t:Rebuild
rem if "%1" == "analyze" set Analyze=/p:EnablePREfast=true /p:RunCodeAnalysis=true /p:CodeAnalysisRuleSet=NativeRecommendedRules.ruleset
if "%1" == "analyze" set Analyze=/p:EnablePREfast=true /p:RunCodeAnalysis=true /p:CodeAnalysisRuleSet=AllRules.ruleset
if "%1" == "2012" set PlatformToolset=v110
if "%1" == "2013" set PlatformToolset=v120
if "%1" == "2015" set PlatformToolset=v140
if "%1" == "2017" set PlatformToolset=v141
goto :eof

:dobuild

msbuild ..\vs\bb.sln %Rebuild% /p:Configuration=%Configuration% /p:PlatformToolset=%PlatformToolset% /m %Analyze%
