// Copyright (c) 2012-2017 Matt Campbell
// MIT license (see License.txt)

#include "common/bb_common.h"
#include <stdio.h>
#include <stdlib.h>

BB_WARNING_DISABLE(4710) // fprintf not inlined - can't push/pop because it happens later

/*
#include <dirent.h>
int listdir( const char *path )
{
	struct dirent *entry;
	DIR *dp;

	dp = opendir( path );
	if( dp == NULL )
	{
		perror( "opendir" );
		return -1;
	}

	while( ( entry = readdir( dp ) ) )
		puts( entry->d_name );

	closedir( dp );
	return 0;
}

int main( int argc, char **argv ) {
	int counter = 1;

	if( argc == 1 )
		listdir( "." );

	while( ++counter <= argc ) {
		printf( "\nListing %s...\n", argv[counter - 1] );
		listdir( argv[counter - 1] );
	}

	return 0;
}
*/

typedef char *files_entry_t;

typedef struct files_s {
	u32 count;
	u32 allocated;
	files_entry_t *names;
} files_t;

int compareFiles(const void *a_, const void *b_)
{
	const files_entry_t *a = (const files_entry_t *)a_;
	const files_entry_t *b = (const files_entry_t *)b_;
	return strcmp(*a, *b);
}

void add_file(files_t *files, const char *dir, const char *filename)
{
	if(files->count == files->allocated) {
		u32 allocated = files->count * 2 + 1;
		files_entry_t *names = (files_entry_t *)malloc(allocated * sizeof(files_entry_t));
		if(!names)
			return;

		memcpy(names, files->names, sizeof(files_entry_t) * files->count);
		memset(names + files->count, 0, sizeof(files_entry_t) * (allocated - files->count));
		free(files->names);
		files->names = names;
		files->allocated = allocated;
	}

	char temp[1024];
	if(dir && *dir) {
		if(snprintf(temp, sizeof(temp), "%s/%s", dir, filename) < 0) {
			temp[sizeof(temp) - 1] = '\0';
		}
	} else {
		strncpy(temp, filename, sizeof(temp));
		temp[sizeof(temp) - 1] = '\0';
	}
	files->names[files->count++] = _strdup(temp);
}

void add_files(files_t *files, const char *dir, const char *filter, const char **excludes)
{
	WIN32_FIND_DATA find;
	HANDLE hFind;
	if(INVALID_HANDLE_VALUE != (hFind = FindFirstFileA(filter, &find))) {
		do {
			bool add = true;
			if(excludes) {
				const char **exclude = excludes;
				while(*exclude) {
					if(!_stricmp(*exclude, find.cFileName)) {
						add = false;
					}
					++exclude;
				}
			}

			if(add) {
				add_file(files, dir, find.cFileName);
			}
		} while(FindNextFileA(hFind, &find));
		FindClose(hFind);
	}
}

bool write_files(files_t *files, const char *path)
{
	FILE *fp = fopen(path, "wb");
	if(fp) {
		for(u32 i = 0; i < files->count; ++i) {
			fprintf(fp, "#include \"%s\"\n", files->names[i]);
		}
		fclose(fp);
		return true;
	}
	return false;
}

int main(int argc, const char **argv)
{
	BB_UNUSED(argc);
	BB_UNUSED(argv);

	{
		const char *excludes[] = {
			//"bb_discovery_server.c",
			NULL
		};
		files_t commonFiles = { 0 };
		add_files(&commonFiles, "common", "common\\*.c", excludes);
		qsort(commonFiles.names, commonFiles.count, sizeof(commonFiles.names[0]), compareFiles);
		if(!write_files(&commonFiles, "bbcommon_unity.c")) {
			return 2;
		}
	}

	return 0;
}
