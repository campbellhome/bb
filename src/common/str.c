// Copyright (c) 2012-2019 Matt Campbell
// MIT license (see License.txt)

#include "str.h"

#include <stdlib.h>

u32 strtou32(const char *s)
{
	return atoi(s);
}

s32 strtos32(const char *s)
{
	return atoi(s);
}
