// Copyright (c) 2012-2019 Matt Campbell
// MIT license (see License.txt)

#pragma once

#include "sb.h"

#if defined(__cplusplus)
extern "C" {
#endif

const char *path_get_filename(const char *path);
sb_t path_resolve(sb_t src);
void path_resolve_inplace(sb_t *path);
void path_remove_filename(sb_t *path);
b32 path_mkdir(const char *path);
b32 path_rmdir(const char *path); // non-recursive, must be empty

#if defined(__cplusplus)
}
#endif
