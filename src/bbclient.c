// Copyright (c) 2012-2019 Matt Campbell
// MIT license (see License.txt)

#if defined(_MSC_VER)
__pragma(warning(disable : 4710)); // warning C4710 : 'int printf(const char *const ,...)' : function not inlined
#endif

#include "bb.c"
#include "bb_array.c"
#include "bb_assert.c"
#include "bb_connection.c"
#include "bb_criticalsection.c"
#include "bb_discovery_client.c"
#include "bb_discovery_packet.c"
#include "bb_file.c"
#include "bb_log.c"
#include "bb_packet.c"
#include "bb_serialize.c"
#include "bb_sockets.c"
#include "bb_string.c"
#include "bb_time.c"

#include "bb_wrap_stdio.h"
#if defined(_MSC_VER)
__pragma(warning(disable : 4820 4255 4668 4574));
#include <windows.h>
#else
#include <stdint.h>
#endif

#include "bb_time.h"

static const char *s_categoryNames[] = {
	"dynamic::category::thing",
	"dynamic::monkey::banana",
};

static const char *s_pathNames[] = {
	"C:\\Windows\\System32\\user32.dll",
	"C:\\Windows\\Fonts\\Consola.ttf",
	"D:\\bin\\wsl-terminal\\vim.exe",
};

static b32 s_bQuit = false;

static void console_command_handler(const char *text, void *context)
{
	(void)context;
	BB_LOG("Console", "^:] %s\n", text);
	if(!bb_stricmp(text, "quit")) {
		s_bQuit = true;
	}
}

int main(int argc, const char **argv)
{
	uint64_t start = bb_current_time_ms();
	uint32_t categoryIndex = 0;
	uint32_t pathIndex = 0;
	(void)argc;
	(void)argv;

	//bb_init_file("bbclient.bbox");
	//BB_INIT("bbclient: matt");
	BB_INIT_WITH_FLAGS("bbclient: matt", kBBInitFlag_ConsoleCommands);
	//BB_INIT_WITH_FLAGS("bbclient: matt (no view)", kBBInitFlag_NoOpenView);
	BB_THREAD_START("main thread!");
	BB_LOG("startup", "bbclient init took %llu ms", bb_current_time_ms() - start);

	BB_SET_INCOMING_CONSOLE_COMMAND_HANDLER(&console_command_handler, NULL);

	BB_LOG("test::category::deep", "this is a __%s__ test at time %zu", "simple", bb_current_time_ms());
	BB_WARNING("test::other", "monkey");
	BB_LOG("test::a", "fred");
	BB_LOG("testa::bob", "george");
	BB_ERROR("testa", "chuck");
	BB_LOG("standalone::nested::category", "standalone::nested::category");

	start = bb_current_time_ms();
	while(BB_IS_CONNECTED()) {
		bb_sleep_ms(160);
		BB_TICK();

		BB_LOG("test::category", "new frame");

		BB_LOG_DYNAMIC(s_pathNames[pathIndex++], 1001, s_categoryNames[categoryIndex++], "This is a %s test!\n", "**DYNAMIC**");
		if(pathIndex >= BB_ARRAYSIZE(s_pathNames)) {
			pathIndex = 0;
		}
		if(categoryIndex >= BB_ARRAYSIZE(s_categoryNames)) {
			categoryIndex = 0;
		}

		if(bb_current_time_ms() - start > 300) {
			break;
		}
	}

	while(BB_IS_CONNECTED() && !s_bQuit) {
		BB_TICK();
		bb_sleep_ms(50);
	}

	while(BB_IS_CONNECTED() && !s_bQuit) {
		BB_TICK();
		BB_LOG("test::frame", "-----------------------------------------------------");
		BB_LOG("test::spam1", "some data");
		BB_LOG("test::spam1", "or other");
		BB_LOG("test::spam1", "spamming");
		BB_LOG("test::spam1", "many times");
		BB_LOG("test::spam1", "in the");
		BB_LOG("test::spam1", "same frame");
		BB_LOG("test::spam1", "over and");
		BB_LOG("test::spam1", "over.");
		BB_LOG("test::spam2", "some data");
		BB_LOG("test::spam2", "or other");
		BB_LOG("test::spam2", "spamming");
		BB_LOG("test::spam2", "many times");
		BB_LOG("test::spam2", "in the");
		BB_LOG("test::spam2", "same frame");
		BB_LOG("test::spam2", "over and");
		BB_LOG("test::spam2", "over.");
		BB_LOG("test::spam3", "some data");
		BB_LOG("test::spam3", "or other");
		BB_LOG("test::spam3", "spamming");
		BB_LOG("test::spam3", "many times");
		BB_LOG("test::spam3", "in the");
		BB_LOG("test::spam3", "same frame");
		BB_LOG("test::spam3", "over and");
		BB_LOG("test::spam3", "over.");
		bb_sleep_ms(50);
		if(bb_current_time_ms() - start > 1000) {
			break;
		}
	}

	BB_SHUTDOWN();
	return 0;
}
