// Copyright (c) 2012-2019 Matt Campbell
// MIT license (see License.txt)

#if defined(_MSC_VER)
__pragma(warning(disable : 4710)); // warning C4710 : 'int printf(const char *const ,...)' : function not inlined
#endif

#define BB_ENABLED 1

//#include "../common/file_utils.c"
//#include "../common/process_utils.c"

#include "../common/appdata.c"
#include "../common/bb_thread.c"
#include "../common/cmdline.c"
#include "../common/env_utils.c"
#include "../common/filter.c"
#include "../common/message_box.c"
#include "../common/output.c"
#include "../common/path_utils.c"
//#include "../common/process_task.c"
#include "../common/sb.c"
#include "../common/sdict.c"
#include "../common/span.c"
#include "../common/str.c"
#include "../common/tasks.c"
#include "../common/tokenize.c"
#include "../common/va.c"

// include bb client code directly for unity compile speedup over 2x
#include "../client/bb.c"
#include "../client/bb_array.c"
#include "../client/bb_assert.c"
#include "../client/bb_connection.c"
#include "../client/bb_criticalsection.c"
#include "../client/bb_discovery_client.c"
#include "../client/bb_discovery_packet.c"
#include "../client/bb_file.c"
#include "../client/bb_log.c"
#include "../client/bb_packet.c"
#include "../client/bb_serialize.c"
#include "../client/bb_sockets.c"
#include "../client/bb_string.c"
#include "../client/bb_time.c"

#include "bb.h"


int main(int argc, const char **argv)
{
	BB_INIT("linuxtest");
	BB_THREAD_START("main");
	cmdline_init(argc, argv);

	BB_LOG("startup", "%s", cmdline_get_full());

	sb_t home = env_get("HOME");
	BB_LOG("startup", "HOME: %s", sb_get(&home));
	sb_reset(&home);

	cmdline_shutdown();
	BB_SHUTDOWN();
	return 0;
}
