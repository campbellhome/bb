@echo off

setlocal
set UE_SYSROOT=../../../../Extras/ThirdPartyNotUE/SDKs/HostWin64/Linux_x64/v11_clang-5.0.0-centos7/x86_64-unknown-linux-gnu
set UE_OPTS=-nostdinc++ -I%UE_SYSROOT%/usr/include/ -gdwarf-4 -g -glldb -fstandalone-debug -O2 -fno-exceptions -DPLATFORM_EXCEPTIONS_DISABLED=1 -D_LINUX64 -target x86_64-unknown-linux-gnu --sysroot="%UE_SYSROOT%"
call ..\..\..\..\Extras\ThirdPartyNotUE\SDKs\HostWin64\Linux_x64\v11_clang-5.0.0-centos7\x86_64-unknown-linux-gnu\bin\clang.exe %UE_OPTS% -g -Werror -Wall -Wextra -I../include -Iclient -DBB_WIDECHAR=0 bboxtolog/bboxtolog.c client/*.c -o ..\linux\bboxtolog
copy ..\linux\bboxtolog ..\linux\bbcat
copy ..\linux\bboxtolog ..\linux\bbtail
